# wccmczech-web

## Install website on server

For detailed install guide see [this wordpress howto](https://wordpress.org/support/article/how-to-install-wordpress)

1. Create database in MariaDB - see [create script](./mariaDB/wccmczech-setup.sql)

2. Download wordpress tarball and unpack it e.g. in ~/public_html/wccmczech/
  * [Download latest tarball (CS)](https://cs.wordpress.org/latest-cs_CZ.tar.gz)
  * after unpacking there should exist directory `~/public_html/wccmczech/wordpress/` with bunch of php and other files

3. Set up apache - see this [apache conf](./apache/wccmczech.conf)
  * also make sure you have php installed and enabled in apache

4. Register your site's hostname
  * Add this line to your `/etc/hosts`
  ```
  127.0.0.1 _wccmczech
  ```

5. Edit **wp.config.php** (inside unpacked wordpress directory)
  * Change these settings as follows
    ```
    /** The name of the database for WordPress */
    define( 'DB_NAME', 'wccmczech' );

    /** MySQL database username */
    define( 'DB_USER', 'wccmczech' );

    /** MySQL database password */
    define( 'DB_PASSWORD', 'wccmczech' );

    /** MySQL hostname */
    define( 'DB_HOST', '127.0.0.1' );
    ```
    * it is particularly important to __set localhost as 127.0.0.1__

  * On some php configurations it is needed to set also this
    ```
    ini_set("pcre.jit", "0");
    ```

  * Generate and set the authentication keys according descriptions (the `*_KEY` defines)

6. Access your wordpress admin site [https://_wccmczech.cz/wp-admin](https://_wccmczech.cz/wp-admin)

## Update wordpress

1. Download wordpress tarball and unpack it e.g. in ~/public_html/wccmczech/
  * [Download latest tarball (CS)](https://cs.wordpress.org/latest-cs_CZ.tar.gz)
  * unpack

2. Synchronize fully all files (remove obsolete in target if necessary) in these paths:
  * /wp-admin/
  * /wp-includes/
  * /*{.php,.txt,.html} (DO NOT remove wp-config.php)
  * /wp-content/index.php
  * /wp-content/languages/
  * /wp-content/plugins/index.php
  * /wp-content/themes/twentytwentyone/
