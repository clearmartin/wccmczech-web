# Dump
$ mariadb-dump -u wccm_czech_next -pwccm_czech_next wccm_czech_next > "wccm_czech_next-dump-`date --iso-8601=seconds`.sql"

# Import
$ mariadb -u wccm_czech_next -p wccm_czech_next < wccmczech-dump.sql
