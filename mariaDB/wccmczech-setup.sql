CREATE DATABASE wccm_czech_next DEFAULT CHARACTER SET utf8;
GRANT ALL PRIVILEGES ON wccm_czech_next.* to 'wccm_czech_next'@'localhost' IDENTIFIED BY 'wccm_czech_next';

--DROP DATABASE wccm_czech_next;
--REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'wccm_czech_next'@'localhost';
--DROP USER 'wccm_czech_next'@'localhost';
