<?php
add_action( 'wp_enqueue_scripts', 'enqueue_child_styles' );
function enqueue_child_styles() {
    wp_enqueue_style( 'twenty-twenty-one-child-style', get_template_directory_uri().'-child/style-child.css', array(), wp_get_theme()->get( 'Version' ));
}
//load_theme_textdomain( 'twentytwentyone', get_template_directory() . '-child/languages' );
?>
